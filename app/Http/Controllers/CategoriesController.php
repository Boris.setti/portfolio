<?php

namespace App\Http\Controllers;

use App\Categorie;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $value = request('name');

        $categorie = new Categorie();
        $categorie->name = $value;
        $categorie->slug = Str::slug($value, '-');
        $categorie->save();
        return back()->with('success', 'Catégorie ajouté avec succès!');
    }


    public function delete()
    {

        DB::table('categories')->where('id', '=', request('id'))->delete();
        return back()->with('success', 'Projet supprimé avec succès!');
    }
    public function update()
    {

        DB::table('categories')
        ->where('id','=', request('id'))
        ->update([
            'name' => request('name'),
            'slug'=>Str::slug(request('name'),'-')
            ]);

            return back()->with('success', 'Catégorie modifier avec succès!');
    }
}
