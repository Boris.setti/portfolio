<?php

namespace App\Http\Controllers;

use App\Projet;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
    public function create()
    {
        request()->validate([
            "name" => 'required',
            "description" => 'required',
            "technology" => 'required',
            "categorie_id" => 'required',
            'image_url' => 'sometimes|image|max:5000'
        ]);

        $projet = new Projet();

        if (request('image_url') != null) {
            $projet->image_url = request('image_url')->store('imgProject', 'public');
        } else {
            $projet->image_url = 'imgDefault/default_value.jpg';
        }

        $projet->name = request('name');
        $projet->description = request('description');
        $projet->technology = request('technology');
        $projet->repo_url = request('repo_url');
        $projet->website_url = request('website_url');
        $projet->categorie_id = request('categorie_id');
        $projet->save();
        return redirect("admin/allprojet")->with('success', 'Projet ajouté avec succès!');
    }

    public function delete()
    {
        DB::table('projets')->where('id', '=', request('id'))->delete();
        return back()->with('success', 'Catégorie supprimé avec succès!');
    }

    public function update()
    {

        request()->validate([
            "name" => 'required',
            "description" => 'required',
            "technology" => 'required',
            "categorie_id" => 'required',
            'image_url' => 'sometimes|image|max:5000'
        ]);

        if (request('image_url') == null) {
            DB::table('projets')
                ->where('id', request('id'))
                ->update([
                    'name' => request('name'),
                    'description' => request('description'),
                    'technology' => request('technology'),
                    'repo_url' => request('repo_url'),
                    'website_url' => request('website_url'),
                    'categorie_id' => request('categorie_id'),
                ]);
        } else {
            DB::table('projets')
                ->where('id', request('id'))
                ->update([
                    'name' => request('name'),
                    'description' => request('description'),
                    'image_url' => request('image_url')->store('imgProject', 'public'),
                    'technology' => request('technology'),
                    'repo_url' => request('repo_url'),
                    'website_url' => request('website_url'),
                    'categorie_id' => request('categorie_id'),
                ]);
        }

        return redirect("admin/allprojet")->with('success', 'Projet modifier avec succès!');
    }
}
