<?php

namespace App\Http\Controllers;

use App\Article;
use App\User;
use App\Projet;
use App\Categorie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home()
    {
        $projets = Projet::all();

        $articles = DB::table('articles')
            ->join('users', 'users.id', '=', 'articles.user_id')
            ->select('articles.*', 'users.pseudo')
            ->get();

        return view('admin/home', [
            'projets' => $projets,
            'articles' => $articles
        ]);
    }

    public function categorie()
    {
        $categories = Categorie::all();

        return view('admin/categorie', [
            'categories' => $categories
        ]);
    }

    public function createprojet()
    {
        $categories = Categorie::all();

        return view('admin/createprojet', [
            'categories' => $categories
        ]);
    }
    public function allprojet()
    {
        $projets = Projet::all();

        return view('admin/allprojet', [
            'projets' => $projets
        ]);
    }
    public function showprojet()
    {
        $projets = DB::table('projets')->where('id', '=', request('id'))->get();

        return view('admin/showprojet', [
            'projets' => $projets
        ]);
    }

    public function editeprojet()
    {
        $projets = DB::table('projets')->where('id', '=', request('id'))->get();
        $categories = Categorie::all();

        if (Categorie::where('id', '=', $projets['0']->categorie_id)->exists()) {
            $thisCategorie = DB::table('categories')->where('id', '=', $projets['0']->categorie_id)->get();
        } else {
            $thisCategorie = [];
        }

        return view('admin/editeprojet', [
            'projets' => $projets,
            'categories' => $categories,
            'thisCategorie' => $thisCategorie
        ]);
    }

    public function createarticle()
    {
        $users = User::all();

        return view('admin/createarticle', [
            "users" => $users
        ]);
    }
    public function allarticle()
    {
        $articles = DB::table('articles')
            ->join('users', 'users.id', '=', 'articles.user_id')
            ->select('articles.*', 'users.pseudo')
            ->get();

        return view('admin/allarticle', [
            'articles' => $articles
        ]);
    }
    public function showarticle()
    {
        $articles = DB::table('articles')
            ->where('articles.id', '=', request('id'))
            ->join('users', 'users.id', '=', 'articles.user_id')
            ->get();

        return view('admin/showarticle', [
            'articles' => $articles
        ]);
    }

    public function editearticle()
    {

        $articles = DB::table('articles')
            ->where('articles.id', '=', request('id'))
            ->join('users', 'users.id', '=', 'articles.user_id')
            ->select('articles.*', 'users.pseudo')
            ->get();
        return view('admin/editearticle', [
            'articles' => $articles
        ]);
    }
}
