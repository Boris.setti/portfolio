<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticlesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        request()->validate([
            "title" => 'required',
            "description" => 'required',
            'image_url' => 'sometimes|image|max:5000'
        ]);

        $article = new Article();

        if (request('image_url') != null) {
            $article->image_url = request('image_url')->store('imgArticle', 'public');
        } else {
            $article->image_url = 'imgDefault/default_value.jpg';
        }

        $article->title = request('title');
        $article->description = request('description');
        $article->user_id = request('user_id');
        $article->save();
        return redirect("admin/allarticle")->with('success', 'Article ajouté avec succès!');
    }

    public function delete()
    {
        DB::table('articles')->where('id', '=', request('id'))->delete();
        return back()->with('success', 'Article supprimé avec succès!');
    }

    public function update()
    {
        // dd(request());
        request()->validate([
            "title" => 'required',
            "description" => 'required',
            'image_url' => 'sometimes|image|max:5000'
        ]);

        if (request('image_url') == null) {
            DB::table('articles')
                ->where('id', request('id'))
                ->update([
                    'title' => request('title'),
                    'description' => request('description'),
                ]);
        } else {
            DB::table('articles')
                ->where('id', request('id'))
                ->update([
                    'title' => request('title'),
                    'description' => request('description'),
                    'image_url' => request('image_url')->store('imgArticle', 'public'),
                ]);
        }

        return redirect("admin/allarticle")->with('success', 'Article modifier avec succès!');
    }
}
