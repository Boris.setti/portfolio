@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Créer un article</h1>
 
  <form action="/createarticle" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="form-group">
      <label for="title">Titre de l'article</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror" name="title" id="title" required>
      @error('title')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label for="description">Contenu de l'article</label>
      <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" rows="3" required></textarea>
      @error('description')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="custom-file">
      <input type="file" name="image_url" class="custom-file-input @error('image_url')is-invalid @enderror" id="validatedCustomFile">
      <label class="custom-file-label" for="validatedCustomFile">Image de l'article (max 5mo)</label>
      @error('image_url')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="form-group">
      <label for="user_id">Auteur</label>
      <select class="form-control @error('categorie_id') is-invalid @enderror" id="user_id" name="user_id">
        @foreach($users as $user)

        <option value="{{ $user->id }}">{{ $user->pseudo }}</option>

        @endforeach
      </select>

      @error('user_id')

      <div class="invalid-feedback">
        {{ $message }}
      </div>

      @enderror

      <button type="submit" class="btn btn-primary">Valider</button>
      <a class="btn btn-danger" href="/admin/allarticle">Retour</a>
  </form>



</div>
@endsection