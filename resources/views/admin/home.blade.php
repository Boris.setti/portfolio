@extends('layouts.app')

@section('content')

<div class="container">
  <h1>Page d'accueil admin</h1>

  <div class="d-flex flex-column flex-md-row">

    <div class="container">
      <!-- PROJET -->

      @if($projets == "[]")
      <div class="w-100">
        <h2>Vous n'avez pas de projet</h2>
        <a class="btn btn-success" href="/admin/createprojet">Créer un Projet</a>

      </div>
      @else
      <div class="d-flex justify-content-around">

        <h2>Liste des Projets</h2>
        <a href="/admin/allprojet" class="btn btn-success">Liste des Projet</a>

      </div>
      <div id="carouselProjet" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <?php $loopNumberProjet = 0 ?>
          @foreach($projets as $projet)
          <li data-target="#carouselProjet" data-slide-to="<?php echo $loopNumberProjet ?>" class="active"></li>
          <?php $loopNumberProjet += 1 ?>
          @endforeach
        </ol>
        <div class="carousel-inner">
          {{ $loopActiveItemProjet = 0}}
          @foreach($projets as $projet)
          @if($loopActiveItemProjet == 0)
          <div class="carousel-item active">
            <img style="height:450px;" src="storage/{{ $projet->image_url }}" class="d-block w-100" alt="image du projet">
            <div class="carousel-caption d-none d-md-block">
              <h5>{{ $projet->name }}</h5>
              <p>{{ $projet->description }}</p>
              <td class="form-inline ">
                <form action="/admin/showprojet" method="POST">
                  @csrf

                  <div class="form-group">
                    <input type="hidden" name="id" value="{{ $projet->id }}">
                  </div>

                  <button type="submit" class="btn btn-secondary">Aperçu</button>
                </form>
            </div>
          </div>
          {{ $loopActiveItemProjet = 1 }}
          @else
          <div class="carousel-item">
            <img style="height:450px;" src="storage/{{ $projet->image_url }}" class="d-block w-100" alt="image du projet">
            <div class="carousel-caption d-none d-md-block">
              <h5>{{ $projet->name }}</h5>
              <p>{{ $projet->description }}</p>
              <form action="/admin/showprojet" method="POST">
                @csrf

                <div class="form-group">
                  <input type="hidden" name="id" value="{{ $projet->id }}">
                </div>

                <button type="submit" class="btn btn-secondary">Aperçu</button>
              </form>
            </div>
          </div>
          @endif

          @endforeach



        </div>
        <a class="carousel-control-prev" href="#carouselProjet" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselProjet" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      @endif
    </div> <!-- ENDPROJET -->

    <div class="container">
      <!-- ARTICLE -->


      @if($articles == "[]")
      <div class="w-100">
        <h2>Vous n'avez pas d'article</h2>
        <a class="btn btn-success" href="/admin/createarticle">Créer un article</a>

      </div>
      @else
      <div class="d-flex justify-content-around">
        <h2>Liste des Articles</h2>
        <a href="/admin/allarticle" class="btn btn-success">Liste des articles</a>

      </div>


      <div id="carouselArticle" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <?php $loopNumberActicle = 0 ?>
          @foreach($articles as $article)
          <li data-target="#carouselArticle" data-slide-to="<?php echo $loopNumberActicle ?>" class="active"></li>
          <?php $loopNumberActicle += 1 ?>
          @endforeach
        </ol>
        <div class="carousel-inner">
          {{ $loopActiveItemActicle = 0}}
          @foreach($articles as $article)
          @if($loopActiveItemActicle == 0)
          <div class="carousel-item active">
            <img style="height:450px;" src="storage/{{ $article->image_url }}" class="d-block w-100" alt="image de l'article">
            <div class="carousel-caption d-none d-md-block">
              <h5>{{ $article->title }}</h5>
              <p>{{ $article->description }}</p>
              <td class="form-inline ">
                <form action="/admin/showarticle" method="POST">
                  @csrf

                  <div class="form-group">
                    <input type="hidden" name="id" value="{{ $article->id }}">
                  </div>

                  <button type="submit" class="btn btn-secondary">Aperçu</button>
                </form>
            </div>
          </div>
          {{ $loopActiveItemActicle = 1 }}
          @else
          <div class="carousel-item">
            <img style="height:450px;" src="storage/{{ $article->image_url }}" class="d-block w-100" alt="image du article">
            <div class="carousel-caption d-none d-md-block">
              <h5>{{ $article->title }}</h5>
              <p>{{ $article->description }}</p>
              <form action="/admin/showarticle" method="POST">
                @csrf

                <div class="form-group">
                  <input type="hidden" name="id" value="{{ $article->id }}">
                </div>

                <button type="submit" class="btn btn-secondary">Aperçu</button>
              </form>
            </div>
          </div>
          @endif

          @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselArticle" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselArticle" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      @endif
    </div> <!-- ENDARTICLES -->
  </div>

</div>
@endsection