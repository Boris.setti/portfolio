@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Créer un Projet</h1>
    @if ($categories != '[]')

    <form action="/createproject" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <label for="name">Nom du projet</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" required>
            @error('name')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" rows="3" required></textarea>
            @error('description')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="custom-file">
            <input type="file" name="image_url" class="custom-file-input @error('image_url')is-invalid @enderror" id="validatedCustomFile">
            <label class="custom-file-label" for="validatedCustomFile">Image du projet (max 5mo)</label>
            @error('image_url')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="technology">Technologie</label>
            <input type="text" class="form-control @error('technology') is-invalid @enderror" name="technology" id="technology" required>
            @error('technology')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="repo_url">URL du repo</label>
            <input type="text" class="form-control" name="repo_url" id="repo_url">
        </div>

        <div class="form-group">
            <label for="website_url">URL en ligne</label>
            <input type="text" class="form-control" name="website_url" id="website_url">
        </div>


        <div class="form-group">
            <label for="categorie_id">Catégorie</label>
            <select class="form-control @error('categorie_id') is-invalid @enderror" id="categorie_id" name="categorie_id">
                @foreach($categories as $categorie)

                <option value="{{ $categorie->id }}">{{ $categorie->name }}</option>

                @endforeach
            </select>

            @error('categorie_id')

            <div class="invalid-feedback">
                {{ $message }}
            </div>

            @enderror
        </div>

        <button type="submit" class="btn btn-primary">Valider</button>
        <a class="btn btn-danger" href="/admin/allprojet">Retour</a>
    </form>


    @else

    <p>Vous devez avoir au moins une catégorie pour créer un projet</p>
    <a class="btn btn-success" href="/admin/categorie"> Créer un catégorie </a>
    @endif
</div>
@endsection