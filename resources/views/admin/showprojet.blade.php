@extends('layouts.app')

@section('content')

<div class="container ">
  @foreach( $projets as $projet )
  <h1>{{ $projet->name }}</h1>

  <div class="card ">
    <img src="/storage/{{ $projet->image_url }}" class="card-img-top" alt="image du Projet">

    <div class="card-body">
      <h4 class="cart-text">Description</h4>
      <p class="card-text">{{ $projet->description }}</p>
      <h4 class="cart-text">Technologie</h4>
      <p class="card-text">{{ $projet->technology }}</p>
    </div>

  </div>
  <div class="d-flex justify-content-around">

    @if($projet->repo_url)

    <p>lien du repo:<a href="HTTP://{{ $projet->repo_url }}">{{ $projet->repo_url }}</a></p>

    @endif

    @if($projet->website_url)

    <p>lien du site:<a href="HTTP://{{ $projet->website_url }}">{{ $projet->website_url }}</a></p>

    @endif

  </div>

  <a class="btn btn-danger" href="/admin/allprojet">Retour</a>
</div>

@endforeach
@endsection