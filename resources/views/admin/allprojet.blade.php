@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Liste des Projets enregistrés</h1>
  <a class="btn btn-success" href="/admin/createprojet">Créer un Projet</a>

  @if ($projets != '[]')

  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">action</th>
        <th scope="col">id</th>
        <th scope="col">name</th>
        <th scope="col">description</th>
        <th scope="col">image</th>
        <th scope="col">repo_url</th>
        <th scope="col">website_url</th>
        <th scope="col">categorie_id</th>
      </tr>
    </thead>
    <tbody>

      @foreach($projets as $projet)

      <tr>

        <td class="form-inline ">
          <form action="/admin/showprojet" method="POST">
            @csrf

            <div class="form-group">
              <input type="hidden" name="id" value="{{ $projet->id }}">
            </div>

            <button type="submit" class="btn btn-primary">Aperçu</button>
          </form>

          <form action="/deleteproject" method="POST">
            @csrf

            <div class="form-group">
              <input type="hidden" name="id" value="{{ $projet->id }}">
            </div>

            <button type="submit" class="btn btn-danger">Supprimer</button>
          </form>

          <form action="/admin/editeprojet" method="POST">
            @csrf

            <div class="form-group">
              <input type="hidden" name="id" value="{{ $projet->id }}">
            </div>

            <button type="submit" class="btn btn-warning">Éditer</button>
          </form>

        </td>

        <th scope="row">{{ $projet->id }}</th>
        <td>
          <div class="overflow-auto" style="width: 200px; height: 100px;">{{ $projet->name }}</div>
        </td>

        <td>
          <div class="overflow-auto" style="width: 200px; height: 100px;">{{ $projet->description }}</div>
        </td>

        <td>
          <div class="card" style="width: 10rem;">
            <img src="/storage/{{ $projet->image_url }}" class="card-img-top" alt="image du Projet">
          </div>
        </td>

        <td>
          <div class="overflow-auto" style="width: 200px; height: 100px;">{{ $projet->repo_url }}</div>
        </td>
        <td>
          <div class="overflow-auto" style="width: 200px; height: 100px;">{{ $projet->website_url }}</div>
        </td>

        <td>{{ $projet->categorie_id }}</td>
      </tr>

      @endforeach

    </tbody>

  </table>

  @else

  <p>Vous n'avez pas de projet</p>

  @endif

</div>

@endsection