@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Edition de l'article {{ $articles['0']->title }} de {{ $articles['0']->pseudo }}</h1>
    @foreach( $articles as $article )
    <form action="/updatearticle" method="POST" enctype="multipart/form-data">
        @csrf

        <input type="hidden" name="id" value="{{ $article->id }}">
        <div class="form-group">
            <label for="title">Nom de l'article</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" name="title" id="title" value="{{ $article->title }}" required>
            @error('name')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class=" form-group">
            <label for="description">Contenu de l'article</label>
            <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" rows="3" required>{{ $article->description }} </textarea>
            @error('description')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="custom-file">
            <input type="file" name="image_url" class="custom-file-input @error('image_url')is-invalid @enderror" id="validatedCustomFile">
            <label class="custom-file-label" for="validatedCustomFile">Image de l'article (max 5mo)</label>
            @error('image_url')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        
        <button type="submit" class="btn btn-primary">Valider</button>
        <a class="btn btn-danger" href="/admin/allarticle">Retour</a>

    </form>
    @endforeach

</div>

@endsection