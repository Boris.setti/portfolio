@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Liste des Articles enregistrés</h1>
  <a class="btn btn-success" href="/admin/createarticle">Créer un Article</a>

  @if ($articles != '[]')

  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">action</th>
        <th scope="col">id</th>
        <th scope="col">titre</th>
        <th scope="col">description</th>
        <th scope="col">image</th>
        <th scope="col">auteur</th>
      </tr>
    </thead>
    <tbody>

      @foreach($articles as $article)

      <tr>

        <td class="form-inline ">
          <form action="/admin/showarticle" method="POST">
            @csrf

            <div class="form-group">
              <input type="hidden" name="id" value="{{ $article->id }}">
            </div>

            <button type="submit" class="btn btn-primary">Aperçu</button>
          </form>

          <form action="/deletearticle" method="POST">
            @csrf

            <div class="form-group">
              <input type="hidden" name="id" value="{{ $article->id }}">
            </div>

            <button type="submit" class="btn btn-danger">Supprimer</button>
          </form>

          <form action="/admin/editearticle" method="POST">
            @csrf

            <div class="form-group">
              <input type="hidden" name="id" value="{{ $article->id }}">
            </div>

            <button type="submit" class="btn btn-warning">Éditer</button>
          </form>

        </td>

        <th scope="row">{{ $article->id }}</th>
        <td>
          <div class="overflow-auto" style="width: 200px; height: 100px;">{{ $article->title }}</div>
        </td>

        <td>
          <div class="overflow-auto" style="width: 200px; height: 100px;">{{ $article->description }}</div>
        </td>

        <td>
          <div class="card" style="width: 10rem;">
            <img src="/storage/{{ $article->image_url }}" class="card-img-top" alt="image de l'article">
          </div>
        </td>

        <td>{{ $article->pseudo }}</td>
      </tr>

      @endforeach

    </tbody>

  </table>

  @else

  <p>Vous n'avez pas d'article</p>

  @endif


</div>
@endsection