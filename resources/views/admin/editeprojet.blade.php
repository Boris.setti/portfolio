@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Edition du projet {{ $projets['0']->name }}</h1>
    @foreach( $projets as $projet )
    <form action="/updateproject" method="POST" enctype="multipart/form-data">
        @csrf

        <input type="hidden" name="id" value="{{ $projet->id }}">
        <div class="form-group">
            <label for="name">Nom du projet</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{ $projet->name }}" required>
            @error('name')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class=" form-group">
            <label for="description">Description</label>
            <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" rows="3" required>{{ $projet->description }} </textarea>
            @error('description')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="custom-file">
            <input type="file" name="image_url" class="custom-file-input @error('image_url')is-invalid @enderror" id="validatedCustomFile">
            <label class="custom-file-label" for="validatedCustomFile">Image du projet (max 5mo)</label>
            @error('image_url')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="technology">Technologie</label>
            <input type="text" class="form-control @error('technology') is-invalid @enderror" name="technology" id="technology" value="{{ $projet->technology }}" required>
            @error('technology')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="repo_url">URL du repo</label>
            <input type="text" class="form-control" name="repo_url" id="repo_url" value="{{ $projet->repo_url }}">
        </div>

        <div class="form-group">
            <label for="website_url">URL en ligne</label>
            <input type="text" class="form-control" name="website_url" id="website_url" value="{{ $projet->website_url }}">
        </div>


        <div class="form-group">
            <label for="categorie_id">Catégorie</label>
            <select class="form-control @error('categorie_id') is-invalid @enderror" id="categorie_id" name="categorie_id" required>
                @if($thisCategorie == [])
                <option value="">effacer</option>
                @else
                <option value="{{ $thisCategorie['0']->id }}">{{$thisCategorie['0']->name }}</option>
                @endif
                @foreach($categories as $categorie)

                <option value="{{ $categorie->id }}">{{ $categorie->name }}</option>

                @endforeach

            </select>
            @error('categorie_id')
            <div class="invalid-feedback">
                {{ $message }}
            </div>

            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Valider</button>
        <a class="btn btn-danger" href="/admin/allprojet">Retour</a>

    </form>
    @endforeach

</div>

@endsection