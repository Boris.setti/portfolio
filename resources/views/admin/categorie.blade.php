@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Page du CRUD Categories</h1>

    @if ($categories != '[]')
    <p>Liste des Catégories enregistrée</p>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">name</th>
                <th scope="col">slug</th>
                <th scope="col">action</th>

            </tr>
        </thead>
        <tbody>

            @foreach($categories as $categorie)
            <tr>
                <th scope="row">{{ $categorie->id }}</th>
                <td>{{ $categorie->name }}</td>
                <td>{{ $categorie->slug }}</td>
                <td class="form-inline ">

                    <form action="/deletecategorie" method="POST">
                        @csrf
                        <div class="form-group">

                            <input type="hidden" name="id" value="{{ $categorie->id }}">
                        </div>

                        <button type="submit" class="btn btn-danger">Supprimer</button>
                    </form>

                    <form class="form-inline" action="/updatecategorie" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" id="exampleFormControlInput1">
                            <input type="hidden" name="id" value="{{ $categorie->id }}">
                        </div>

                        <button type="submit" class="btn btn-warning">Éditer</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <p>Vous n'avez pas de catégorie</p>

    @endif
    <form action="/createcategorie" method="POST">
        @csrf
        <div class="form-group">
            <label for="exampleFormControlInput1">Ajouter une catégorie</label>
            <input type="text" name="name" class="form-control" id="exampleFormControlInput1">
        </div>

        <button type="submit" class="btn btn-primary">Valider</button>
    </form>
</div>
@endsection