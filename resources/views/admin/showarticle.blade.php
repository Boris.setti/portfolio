@extends('layouts.app')

@section('content')

<div class="container ">
  @foreach( $articles as $article )
  <h1>{{ $article->title }}</h1>

  <div class="card ">
    <img src="/storage/{{ $article->image_url }}" class="card-img-top" alt="image de l'article">

    <div class="card-body">
      <h4 class="cart-text">Description</h4>
      <p class="card-text">{{ $article->description }}</p>
      <h4 class="cart-text">Auteur</h4>
      <p class="card-text">{{ $article->pseudo }}</p>
    </div>

  </div>

  <a class="btn btn-danger" href="/admin/allarticle">Retour</a>
</div>

@endforeach
@endsection