<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','GuestController@home');




Auth::routes();
// route admin
Route::get('/admin', 'AdminController@home');
Route::get('/admin/allprojet', 'AdminController@allprojet');
Route::get('/admin/createprojet', 'AdminController@createprojet');
Route::post('/admin/showprojet', 'AdminController@showProjet');
Route::post('/admin/editeprojet','AdminController@editeprojet');
Route::get('/admin/allarticle', 'AdminController@allarticle');
Route::post('/admin/showarticle', 'AdminController@showarticle');
Route::get('/admin/createarticle', 'AdminController@createarticle');
Route::post('/admin/editearticle','AdminController@editearticle');
Route::get('/admin/categorie','AdminController@categorie');

//route CRUD projet
Route::post('createproject', 'ProjectController@create');
Route::post('deleteproject', 'ProjectController@delete');
Route::post('updateproject', 'ProjectController@update');

//route CRUD categorie
Route::post('createcategorie','CategoriesController@create');
Route::post('deletecategorie','CategoriesController@delete');
Route::post('updatecategorie','CategoriesController@update');

//route CRUD aticle
Route::post('createarticle','ArticlesController@create');
Route::post('deletearticle','ArticlesController@delete');
Route::post('updatearticle','ArticlesController@update');




