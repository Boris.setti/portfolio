## a mettre en place
- crée .env a la racine du projet et copier .env.example dans .env
- crée database.sqlite dans le dossier database
- `composer install && npm install`
- `php artisan key:generate`
- `php artisan migrate`
- `php artisan storage:link`
- `php artisan serve`

  

## utilisation 
 à l'arrivée sur le site il faut aller sur l'url suivant `/register` afin de créer un admin
 une fois l'administrateur créé, pour vous connecter vous devrez aller à l'url suivante `/admin`
 une fois connecté en tant qu'admin, la barre de navigation changera et les onglets pour CRUD apparaîtront
 différents gardes fous ont été mis en place pour éviter les bugs, exemple: si vous voulez créer un projet et qu'il n'existe pas de catégorie, un message vous demandera de les créer d'abord
 d'autres sécurités de ce type ont été mises en place pour parer au maximum d'erreurs possibles
 